let mode
let permission = false
mode = new ModeularJS('modeularjsexample',()=>{
    console.log('Init')
    mode.askForPermission('ping',()=>{
        console.log('Success')
        permission = true

    },()=>{
        console.log('Failure')
    })
})

let ping = document.getElementById('ping')
let pong = document.getElementById('pong')
let timeout = null

ping.addEventListener('keydown', (e)=>{
    clearTimeout(timeout)
    timeout = setTimeout(()=>{
        let text = ping.value
        console.log(text)
        mode.useModule('ping',(res)=>{
            pong.innerHTML = res
        },text)
    },400)

})

