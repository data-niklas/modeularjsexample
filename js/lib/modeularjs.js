class ModeularJS{
    constructor(appname, onInit = ()=>{}){
        this.appname = appname
        this.serverAvailable = false
        this.serverInfo = {}
        this.token = ''
        this.allowedModules = []
        this.checkServer(onInit)
        
    }
    defaultURL(){
        return ModeularJS.urlbase + ModeularJS.port + '/'
    }

    checkServer(onInit=()=>{}){
        let _this = this
        this.sendRequest(this.defaultURL(), (result)=>{
            _this.serverAvailable = true
            _this.serverInfo = JSON.parse(result)
            _this.checkToken(onInit)
        }, ()=>{
            onInit()
        })
    }
    checkToken(onInit=()=>{}){
        if (this.token != ''){
            onInit()
            return
        }
        let token = window.localStorage.getItem('token')
        if (token == null){
            let _this = this
            this.sendRequest(_this.defaultURL() + 'generate/',(result)=>{
                _this.token = result
                window.localStorage.setItem('token',result)
                onInit()
            },()=>{
                onInit()
            })
        }
        else {
            this.token = token
            console.log(this.token)
            onInit()
        }

    }

    checkPermissions(){
        let allowed = window.localStorage.getItem('allowedmodules')
        if (allowed != null)this.allowedModules = JSON.parse(allowed)
        else {
            let _this = this
            this.sendRequest(this.defaultURL() + 'permission/?module=' + modulename + '&token=' + _this.token ,(result)=>{
                if (result.result && result.remember && _this.allowedModules.indexOf(modulename) == -1){
                    _this.allowedModules.push(modulename)
                    window.localStorage.setItem('allowedmodules',JSON.stringify(_this.allowedModules))   
                }
            }, ()=>{
            })
        }
    }
    askForPermission(modulename,success,failure){
        let _this = this
        this.sendRequest(this.defaultURL() + 'askpermission/?module=' + modulename + '&token=' + _this.token + '&name=' + this.appname,(result)=>{
            result = JSON.parse(decodeURIComponent(result))
            if (result.result == true){
                if (_this.allowedModules.indexOf(modulename) == -1){
                    _this.allowedModules.push(modulename)
                    window.localStorage.setItem('allowedmodules',JSON.stringify(_this.allowedModules))
                }
                success()
            }
            else failure()
        }, ()=>{
            failure()
        })
    }

    modulesList(callback){
        this.sendRequest(this.defaultURL() + 'module/', list => {
            console.log(list)
            callback(JSON.parse(list))
        }, ()=>{
            callback(null)
        })
    }

    moduleInfo(modulename,callback){
        this.sendRequest(this.defaultURL() + 'module/?module=' + modulename, info => {
            callback(JSON.parse(info))
        },()=>{
            callback(null)
        })
    }
    useModule(modulename,callback,data='',name=null){
        let string =this.defaultURL() + modulename + '/?token=' + this.token + '&data=' + encodeURIComponent(JSON.stringify(data))
        if (name != null)string += '&name=' + name
        this.sendRequest(string, result => {
            callback(result)
        }, ()=>{
            callback(null)
        })
    }

    sendRequest(url,callback,errcallback=()=>{}){
        var req = new XMLHttpRequest();
        req.open('GET', url, true);
        req.onload  = function() {
            callback(req.response)
        };
        req.onerror = function(){
            errcallback()
        };
        req.send(null);
    }
}
ModeularJS.name = 'modeularjs'
ModeularJS.port = '3000'
ModeularJS.urlbase = 'http://localhost:'
